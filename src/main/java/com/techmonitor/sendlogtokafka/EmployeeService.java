package com.techmonitor.sendlogtokafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    private static Logger LOG = LoggerFactory.getLogger(EmployeeService.class);

    public Employee getEmployee() {
        LOG.info("getting a new Employee");
        return new Employee("Jason",1000.0,"R&D"
        );
    }

}
