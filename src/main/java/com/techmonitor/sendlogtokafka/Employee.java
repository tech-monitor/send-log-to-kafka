package com.techmonitor.sendlogtokafka;

public class Employee {
    private String name;
    private double paid;
    private String department;
    public Employee(String name, double paid, String department) {
        super();
        this.name = name;
        this.paid = paid;
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPaid() {
        return paid;
    }

    public void setPaid(double paid) {
        this.paid = paid;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Employee [name=" + name + ", paid=" + paid + ", department=" + department + "]";
    }
    
    
    
   

}
