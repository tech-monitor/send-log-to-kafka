package com.techmonitor.sendlogtokafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@SpringBootApplication
public class SendLogToKafkaApplication {

	private static Logger LOG = LoggerFactory.getLogger(SendLogToKafkaApplication.class);
	@Autowired
	private EmployeeService employeeService;


	public static void main(String[] args) {
		SpringApplication.run(SendLogToKafkaApplication.class, args);
	}

	@Bean
	public CommandLineRunner run() {
		return args -> {
			LOG.info("Hello welcome to log sender to kafka at "
					+ DateTimeFormatter.ofPattern("HH:mm:ss").format(LocalDateTime.now()));
			LOG.info("{}",employeeService.getEmployee());
		};


	}

}
